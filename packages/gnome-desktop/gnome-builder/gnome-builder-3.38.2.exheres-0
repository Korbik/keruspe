# Copyright 2012-2013 Marc-Antoine Perennou <Marc-Antoine@Perennou.com>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ] vala [ vala_dep=true ]
require python [ blacklist=2 multibuild=false ]
require gsettings gtk-icon-cache freedesktop-desktop freedesktop-mime meson

SUMMARY="An IDE for writing GNOME-based software"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    sysprof [[ description = [ Support performance profiling via sysprof ] ]]
"

DEPENDENCIES="
    build:
        dev-doc/gtk-doc[>=1.32-r1]
        dev-util/desktop-file-utils
        sys-devel/gettext[>=0.19.8]
        virtual/pkg-config[>=0.22]
    build+run:
        app-spell/enchant:2
        core/json-glib[>=1.2]
        dev-libs/appstream-glib
        dev-libs/glib:2[>=2.65.0]
        dev-libs/jsonrpc-glib:1.0[>=3.29.01][gobject-introspection][vapi]
        dev-libs/libdazzle:1.0[>=3.37.00][gobject-introspection][vapi]
        dev-libs/libpeas:1.0[>=1.22.0]
        dev-libs/libxml2:2.0[>=2.9.0]
        dev-libs/pcre
        dev-libs/pcre2
        dev-libs/template-glib:1.0[>=3.28.0][vapi]
        dev-libs/vte:2.91[>=0.46][gobject-introspection][vapi]
        dev-scm/libgit2
        dev-scm/libgit2-glib:1.0[>=0.28.0.1]
        dev-util/ctags
        dev-util/desktop-file-utils
        gnome-bindings/pygobject:3[>=3.22.0]
        gnome-desktop/devhelp:3.0[>=3.25.1][gobject-introspection(+)]
        gnome-desktop/gobject-introspection[>=1.48.0]
        gnome-desktop/gspell:1[>=1.2.0][gobject-introspection]
        gnome-desktop/gtksourceview:4.0[>=4.0.0][gobject-introspection]
        gnome-desktop/libsoup:2.4[>=2.52.2]
        net-libs/webkit:4.0[>=2.26.0][gobject-introspection]
        sys-apps/flatpak[>=0.8.0]
        sys-devel/bison
        sys-devel/flex
        sys-devel/gettext
        sys-devel/libostree
        sys-devel/libtool
        sys-devel/make
        x11-libs/gtk+:3[>=3.22.26][gobject-introspection]
        x11-libs/pango[>=1.38.0][gobject-introspection]
        sysprof? ( gnome-desktop/sysprof[>=3.37.1] )
    run:
        dev-lang/clang
        dev-lang/llvm[-static(-)]
        dev-lang/rust:*[>=1.17]
        dev-util/valgrind
        sys-devel/cmake
        sys-devel/meson
"

# Tests fail under wayland
RESTRICT="test"

MESON_SRC_CONFIGURE_PARAMS=(
 -Dtracing=false
 -Dprofiling=false
 -Dfusermount_wrapper=false
 -Dtcmalloc=false

 -Dchannel=other

 -Dhelp=false
 -Ddocs=false
 -Dnetwork_tests=false

 # Plugins
 -Dplugin_autotools=true
 -Dplugin_beautifier=true
 -Dplugin_c_pack=true
 -Dplugin_cargo=true
 -Dplugin_clang=true
 -Dplugin_cmake=true
 -Dplugin_codespell=true
 -Dplugin_code_index=true
 -Dplugin_color_picker=true
 -Dplugin_copyright=true
 -Dplugin_ctags=true
 -Dplugin_devhelp=true
 -Dplugin_dspy=true
 -Dplugin_editorconfig=true
 -Dplugin_eslint=true
 -Dplugin_file_search=true
 -Dplugin_flatpak=true
 -Dplugin_gdb=true
 -Dplugin_gettext=true
 -Dplugin_git=true
 -Dplugin_gjs_symbols=true
 -Dplugin_gnome_code_assistance=true
 -Dplugin_gradle=true
 -Dplugin_grep=true
 -Dplugin_gvls=true
 -Dplugin_html_completion=true
 -Dplugin_html_preview=true
 -Dplugin_make=true
 -Dplugin_maven=true
 -Dplugin_meson=true
 -Dplugin_modelines=true
 -Dplugin_newcomers=true
 -Dplugin_notification=true
 -Dplugin_npm=true
 -Dplugin_python_pack=true
 -Dplugin_quick_highlight=true
 -Dplugin_retab=true
 -Dplugin_rls=true
 -Dplugin_rust_analyzer=true
 -Dplugin_rustup=true
 -Dplugin_shellcmd=true
 -Dplugin_spellcheck=true
 -Dplugin_stylelint=true
 -Dplugin_sysroot=true
 -Dplugin_todo=true
 -Dplugin_vala=true
 -Dplugin_valgrind=true
 -Dplugin_vagrant=true
 -Dplugin_words=true
 -Dplugin_xml_pack=true

 -Dplugin_deviced=false
 -Dplugin_glade=false
 -Dplugin_go_langserv=false
 -Dplugin_jedi=false
 -Dplugin_jhbuild=false
 -Dplugin_mono=false
 -Dplugin_phpize=false
 -Dplugin_podman=false
 -Dplugin_qemu=false
 -Dplugin_update_manager=false
 -Dplugin_waf=false
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'sysprof plugin_sysprof'
)

src_install() {
    meson_src_install
    edo find "${IMAGE}" -type d -empty -delete
}

pkg_postinst() {
    gsettings_pkg_postinst
    gtk-icon-cache_pkg_postinst
    freedesktop-desktop_pkg_postinst
    freedesktop-mime_pkg_postinst
}

pkg_postrm() {
    gsettings_pkg_postrm
    gtk-icon-cache_pkg_postrm
    freedesktop-desktop_pkg_postrm
    freedesktop-mime_pkg_postrm
}
